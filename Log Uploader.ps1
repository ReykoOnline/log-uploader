# Version
$version = '4.9'

# Config file
$config_md5 = '644ffe1d8546da630018323b3fe47286'
$config_file = '{"main":{"autoupdate":"","compress_log":"","verbose":"","debug":""},"dpsreport":{"enable":"","url_to_clipboard":"","upload_failures":"","upload_kitty_golem":"","post_in_teamspeak":""},"gw2raidar":{"enable":"","upload_failures":"","category":{"enable":"","default":"","ask_on_start":"","ask_on_upload":""},"tag":{"enable":"","default":"","ask_on_start":"","ask_on_upload":"","selection_preset":"supporter;pdps;cdps;chrono;tank;bs;kiter"}},"logrenamer":{"enable":"","format":""}}'

# Data file
$data_md5 = '1e4254f36ac35fcb5fcf5badce82ef99'
$data_file = '{"main":{"config_version":"","data_version":""},"guildwars2":{"token":""},"dpsreport":{"token":"","teamspeak_token":""},"gw2raidar":{"category":{"temp_stored":""},"tag":{"temp_stored":""},"token":""},"temp_zevtc":"0"}'

# Simple Arc Parse
$req_sap_uri = 'https://github.com/jacob-keller/L0G-101086/releases/download/v3.0.1/simpleArcParse.exe'
$req_sap_ver = 'v2.1.0'
$req_arc_ver = '20190329'

# Update
$update_script = 'try { Stop-Process -Name "Log Uploader";[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12;$log_uploader = Invoke-WebRequest -Uri "https://apps.reyko.online/log_uploader/Log%20Uploader.exe";Set-Content -Path "./Log Uploader.exe" -Encoding Byte -Value $log_uploader.Content -Force;Start-Process "./Log Uploader.exe";Add-Content "./log.txt" "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[Updater] Successful"}catch { (New-Object -ComObject Wscript.Shell).Popup("$error",0,"Log Uploader - Updater",0+48);Add-Content "./log.txt" "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[Updater] $error"}Add-Content "./log.txt" "#######################################################################################################################";Remove-Item ./updater.ps1'

$yes = '1','ja','j','yes','y'
$no = '0','nein','n','no',''

function Update-Data {
    Write-Verbose "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[Update][Data File] Initialized"
    Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[Update][Data File] Initialized"
    try {
        # 4.9
        if($data.dpsreport.teamspeak_token -eq $null) {
            Add-Member -MemberType NoteProperty -InputObject $data.dpsreport -Name teamspeak_token -Value '' -Force
            Write-Verbose "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[Update][Data File] Added ""teamspeak_token"" entry"
            Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[Update][Data File] Added ""teamspeak_token"" entry"
        }
        
        # Placeholder for Future Updates
        
        # Update File md5
        $data.main.data_version = $data_md5
        
        # Write
        $data | ConvertTo-Json | Set-Content './data.json'
        Write-Verbose "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[Update][Data File] Successful"
        Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[Update][Data File] Successful"
    }
    catch {
        (New-Object -ComObject Wscript.Shell).Popup("$error",0,"Log Uploader - Update: Data File",0+48)
        Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[Update][Data File][ERROR] $error"
        $error.clear()
        exit
    }
}

function Update-Config {
    Write-Verbose "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[Update][Config File] Initialized"
    Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[Update][Config File] Initialized"
    try {
        # 4.9
        if($config.main.debug -eq $null) {
            Add-Member -MemberType NoteProperty -InputObject $config.main -Name debug -Value '' -Force
            Write-Verbose "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[Update][Config File] Added ""debug"" entry"
            Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[Update][Config File] Added ""debug"" entry"
        }
        if($config.dpsreport.post_in_teamspeak -eq $null) {
            Add-Member -MemberType NoteProperty -InputObject $config.dpsreport -Name post_in_teamspeak -Value '' -Force
            Write-Verbose "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[Update][Config File] Added ""post_in_teamspeak"" entry"
            Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[Update][Config File] Added ""post_in_teamspeak"" entry"
        }
        if($config.main.zip_log) {
            Add-Member -MemberType NoteProperty -InputObject $config.main -Name compress_log -Value $config.main.zip_log -Force
            $config.main.PSObject.properties.remove('zip_log')
            Write-Verbose "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[Update][Config File] Renamed ""zip_log"" entry to ""compress_log"""
            Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[Update][Config File] Renamed ""zip_log"" entry to ""compress_log"""
        }
        
        # Placeholder for Future Updates
        
        # Update File md5
        $data.main.config_version = $config_md5
        
        #Write
        $config | ConvertTo-Json | Set-Content './config.json'
        $data | ConvertTo-Json | Set-Content './data.json'
        Write-Verbose "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[Update][Config File] Successful"
        Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[Update][Config File] Successful"
    }
    catch {
        (New-Object -ComObject Wscript.Shell).Popup("$error",0,"Log Uploader - Update: Config File",0+48)
        Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[Update][Config File][ERROR] $error"
        $error.clear()
        exit
    }
}

function Create-Data {
    $data = $data_file | ConvertFrom-Json
    $data.main.data_version = $data_md5
    if(Test-Path -Path './config.json') {
        $data.main.config_version = $config_md5
    }
    $data | ConvertTo-Json | Set-Content './data.json'
}

function Create-Config {
    $config_file | ConvertFrom-Json | ConvertTo-Json | Set-Content './config.json'
    $data.main.config_version = $config_md5
    $data | ConvertTo-Json | Set-Content './data.json'
}

function Get-Gw2Token {
    try {
        $gw2token = Read-Host 'Token'
        Invoke-WebRequest -Uri "https://api.guildwars2.com/v2/characters?access_token=$gw2token" | Out-Null
    }
    catch {
        Write-Host 'Token invalid. Please try again.'
        Get-Gw2Token
    }
    return $gw2token
}

function Get-Raidar-Token {
    $user = Read-Host 'Username'
    $pw = Read-Host 'Password' 
    $answer = curl.exe -s -F "username=$user" -F "password=$pw" https://www.gw2raidar.com/api/v2/token | ConvertFrom-Json
    $user = $pw = $null
    if($answer.token) {
        $data.gw2raidar.token = $answer.token
        $data | ConvertTo-Json | Set-Content './data.json'
        clear
        Write-Verbose "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[GW2Raidar] Token set up"
        Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[GW2Raidar] Token set up"
    } else {
        Write-Host "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[GW2Raidar][ERROR] Credentials are not correct"
        Get-Raidar-Token
    }
}

function Start-Log-Uploader {
    $watcher = New-Object System.IO.FileSystemWatcher
    $watcher.Path = ([Environment]::GetFolderPath("MyDocuments")+'\Guild Wars 2\addons\arcdps\arcdps.cbtlogs')
    $watcher.Filter = "*.*"
    $watcher.IncludeSubdirectories = $true
    $watcher.EnableRaisingEvents = $true

    $action = { 
                #### "IN ACTION" FUNCTIONS ####
                function Get-Parse-Data {
                    Write-Verbose "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[SimpleArcParse] Initialized"
                    Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[SimpleArcParse] Initialized"
                    [Console]::OutputEncoding = [Text.UTF8Encoding]::UTF8
                    $sap = .\simpleArcParse.exe json "$path.evtc" | ConvertFrom-Json
                    # CHECK SAP RESULT
                    if($sap -eq $null) {
                        (New-Object -ComObject Wscript.Shell).Popup(" Could not parse any data from log. Please contact developer.",0,"Log Uploader - SimpleArcParse",0+48)
                        Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[SimpleArcParse][ERROR] Could not parse any data from log."
                        Stop-Process -Name 'Log Uploader'
                    }
                    $sap_out = New-Object psobject
                    try {
                        # ARC VERSION CHECK
                        Write-Debug "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[SimpleArcParse][ArcDPS][Version] Current:$($sap.header.arcdps_version) Required:$req_arc_ver"
                        if(!(($sap.header.arcdps_version -replace 'EVTC','') -ge $req_arc_ver)) {
                            (New-Object -ComObject Wscript.Shell).Popup("required ArcDps version not met.`n Please update ArcDps and restart application.",0,"Log Uploader - SimpleArcParse",0+48)
                            Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[SimpleArcParse][ArcDps][ERROR] Required version not met"
                            Stop-Process -Name 'Log Uploader'
                        } else {
                            Write-Verbose "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[SimpleArcParse][ArcDps] Required version met"
                            Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[SimpleArcParse][ArcDps] Required version met"
                        }
                        # DATA EXTRACTION
                        if($sap.boss.success -eq $true) {
                            $state = 'Success'
                        } elseif($sap.boss.success -eq $false) {
                            $state = 'Failure'
                        }
                        Add-Member -MemberType NoteProperty -InputObject $sap_out -Name state -Value $state -Force
                        $duration = $sap.boss.duration
                        Add-Member -MemberType NoteProperty -InputObject $sap_out -Name duration -Value $duration -Force
                        $duration_formated = ("{0:\[mm\mss\sfff\m\s\]}" -f [System.TimeSpan]::FromMilliseconds($duration))
                        Add-Member -MemberType NoteProperty -InputObject $sap_out -Name duration_f -Value $duration_formated -Force
                        if($sap.boss.is_cm -eq 'YES') {
                            $cm = ' (CM)'
                        } else {
                            $cm = ''
                        }
                        if($sap.boss.name -like '* \(CM\)') {
                            $boss = $sap.boss.name
                        } else {
                            $boss = "$($sap.boss.name)$cm"
                        }
                        Add-Member -MemberType NoteProperty -InputObject $sap_out -Name boss -Value $boss -Force
                        function Get-Gw2Account-Data {
                            try {
                                $answer = Invoke-WebRequest -uri "https://api.guildwars2.com/v2/account?access_token=$($data.guildwars2.token)" | ConvertFrom-Json 
                                Write-Debug "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[SimpleArcParse][Gw2 Api][Account] $answer"
                                Write-Verbose "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[SimpleArcParse][Gw2 Api][Account] Successful"
                                Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[SimpleArcParse][Gw2 Api][Account] Successful"
                                return $answer
                            }
                            catch {
                                Write-Verbose "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[SimpleArcParse][Gw2 Api][Account] Failed, retry now"
                                Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[SimpleArcParse][Gw2 Api][Account] Failed, retry now"
                                sleep(5)
                                return Get-Gw2Account-Data
                            }
                        }
                        $userdata = $sap.players | Where-Object {$_.account -eq ((Get-Gw2Account-Data).name)}
                        Add-Member -MemberType NoteProperty -InputObject $sap_out -Name character -Value $userdata.character -Force
                        function Get-Guild-Data {
                            try {
                                Write-Verbose "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[SimpleArcParse][Gw2 Api][Guild] Successful"
                                Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[SimpleArcParse][Gw2 Api][Guild] Successful"
                                return Invoke-WebRequest -Uri "https://api.guildwars2.com/v1/guild_details?guild_id=$($userdata.guid)" | ConvertFrom-Json
                            }
                            catch {
                                Write-Verbose "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[SimpleArcParse][Gw2 Api][Guild] Failed, retry now"
                                Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[SimpleArcParse][Gw2 Api][Guild] Failed, retry now"
                                return Get-Guild-Data
                            }
                        }
                        $guild_data = Get-Guild-Data
                        Add-Member -MemberType NoteProperty -InputObject $sap_out -Name guild_name -Value $guild_data.guild_name -Force
                        Add-Member -MemberType NoteProperty -InputObject $sap_out -Name guild_tag -Value $guild_data.tag -Force
                        Write-Debug "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[SimpleArcParse] Data extracted: Boss: $($sap_out.boss) / Duration: $($sap_out.duration) / State: $($sap_out.state) / Character: $($sap_out.character) / Guild: [$($sap_out.guild_tag)] $($sap_out.guild_name)"
                        Write-Verbose "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[SimpleArcParse] Successful"
                        Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[SimpleArcParse] Successful"
                        return $sap_out
                    }
                    catch {
                        (New-Object -ComObject Wscript.Shell).Popup("$error",0,"Log Uploader - SimpleArcParse",0+48)
                        Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[SimpleArcParse][ERROR] $error"
                        Add-Content './log.txt' "#######################################################################################################################"
                        $error.clear()
                        Stop-Process -Name 'Log Uploader'
                    }
                }
                
                function Compress-Log {
                    Write-Verbose "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[Log Compression] Initialized"
                    Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[Log Compression] Initialized"
                    try {
                        Compress-Archive -CompressionLevel Optimal -Path "$path$extension" -DestinationPath "$path$extension.zip"
                        Remove-Item ($path+$extension) -Force
                        Write-Verbose "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[Log Compression] Successful"
                        Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[Log Compression] Successful"
                        return $extension+'.zip'
                    }
                    catch {
                        Write-Host -ForegroundColor Red "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[Log Compression][ERROR] $error"
                        Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[[Log Compression]][ERROR] $error"
                        $error.clear()
                        return $extension
                    }
                }
                
                function Upload-DPSReport {
                    Write-Verbose "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[DPS Report] Initialized"
                    Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[DPS Report] Initialized"
                    if([int]$sap.duration -gt '20000') {
                        Write-Verbose "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[DPS Report] Log exceeds 20s on duration"
                        Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[DPS Report] Log exceeds 20s on duration"
                        try{
                            if(($sap.boss -like '*Kitty Golem*' -AND $config.dpsreport.upload_kitty_golem -in $yes) -OR ($config.dpsreport.upload_failures -in $yes -AND !($sap.boss -like '*Kitty Golem*')) -OR ($($sap.state) -eq 'SUCCESS')) {
                                Write-Debug "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[DPS Report][Upload] curl.exe -s -F ""file=@$path$extension"" ""https://dps.report/uploadContent?json=1&rotation_weap1=1&generator=ei&userToken=$($data.dpsreport.token)"" | ConvertFrom-Json"                           
                                $answer = curl.exe -s -F "file=@$path$extension" "https://dps.report/uploadContent?json=1&rotation_weap1=1&generator=ei&userToken=$($data.dpsreport.token)" | ConvertFrom-Json
                                if($config.dpsreport.url_to_clipboard -in $yes) {
                                    Set-Clipboard -Value $answer.permalink
                                }
                                Write-Host "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[DPS Report][Upload] Successful $($answer.permalink)"
                                Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[DPS Report][Upload] Successful $($answer.permalink)"
                                if($config.dpsreport.post_in_teamspeak -in $yes) {
                                    try {
                                        if(Get-Process ts3client_win64 -ErrorAction SilentlyContinue) {
                                            Invoke-ClientQuerry -apikey $($data.dpsreport.teamspeak_token) -command "sendtextmessage targetmode=2 msg=[URL]$($answer.permalink)[/URL]"
                                        } else {
                                            Write-Host -ForegroundColor Orange "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[DPS Report][Teamspeak][WARNING] Client not running"
                                            Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[DPS Report][Teamspeak][WARNING] Client not running"
                                        }
                                    }
                                    catch {
                                        Write-Host -ForegroundColor Red "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[DPS Report][Teamspeak][ERROR] $error"
                                        Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[DPS Report][Teamspeak][ERROR] $error"
                                        $error.clear()
                                    }
                                }
                                if($($sap.state) -eq 'SUCCESS') {
                                    Add-Content './dpsreport_success_links.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]") $($answer.permalink)"
                                }
                                return $answer.permalink
                            }
                        }
                        catch {
                            Write-Host -ForegroundColor Red "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[DPS Report][ERROR] $error"
                            Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[DPS Report][ERROR] $error"
                            $error.clear()
                        }
                    } else {
                        Write-Host "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[DPS Report] Log ignored because duration <20s"
                        Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[DPS Report] Log ignored because duration <20s"
                    }
                    
                }
                
                function Upload-GW2Raidar {
                    if(!($sap.boss -like '*Kitty Golem*')) {
                        if([int]$sap.duration -ge '60000') {
                            Write-Verbose "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[GW2Raidar] Initialized"
                            Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[GW2Raidar] Initialized"
                            if($sap.state -eq 'SUCCESS' -OR $config.gw2raidar.upload_failures -in $yes) {
                                # TAG
                                try {
                                    if($config.gw2raidar.tag.ask_on_upload -in $yes -AND $config.gw2raidar.tag.enable -in $yes) {
                                        if(($config.gw2raidar.tag.selection_preset -replace ' ','') -ne '') {
                                            $preset = ($config.gw2raidar.tag.selection_preset).Split(';')
                                            $display = ''
                                            $counter = 1
                                            foreach($tag IN $preset) {
                                                $display += "($counter)$tag "
                                                $counter++
                                            }
                                            Write-Host "Predefinied set of tags: $display"
                                            $input = Read-Host 'Select preset or enter temporary tag'
                                            if($input -match "^\d+$" -AND [int]$input -le $counter) {
                                                $selection = $preset[($input-1)]
                                            } else {
                                                $selection = $input
                                            }
                                        } else {
                                            $selection = Read-Host 'Enter temporary tag'
                                        }
                                        $default_tag = $selection
                                    } elseif($config.gw2raidar.tag.ask_on_start -in $yes -AND ($data.gw2raidar.tag.temp_stored -replace ' ','') -ne '' -AND $config.gw2raidar.tag.enable -in $yes) {
                                        $default_tag = $data.gw2raidar.tag.temp_stored
                                    } elseif($config.gw2raidar.tag.enable -in $yes) {
                                        $default_tag = $config.gw2raidar.tag.default
                                    }
                                    # PLACEHOLDER
                                    Write-Debug "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[GW2Raidar][Tags] $default_tag"
                                    $default_tag = $default_tag -replace '{duration}',$sap.duration_f -replace '{boss}',$($sap.boss) -replace '{boss:l}',($($sap.boss).toLower()) -replace '{boss:u}',($($sap.boss).toUpper()) -replace '{state}',$($sap.state) -replace '{state:l}',($($sap.state).toLower()) -replace '{state:u}',($($sap.state).toUpper()) -replace '{guild}',$($sap.guild_name) -replace '{guild:l}',($($sap.guild_name).toLower()) -replace '{guild:u}',($($sap.guild_name).toUpper()) -replace '{guildtag}',$($sap.guild_tag) -replace '{character}',$($sap.character) -replace '{character:u}',($($sap.character).toUpper()) -replace '{character:l}',($($sap.character).toLower())
                                    # DPS.REPORT URL PLACEHOLDER
                                    Write-Debug "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[GW2Raidar][Tags] $dpsreport_url"
                                    if($default_tag -like '*{dpsreporturl}*' -AND ($dpsreport_url)) {
                                        $default_tag = $default_tag -replace '{dpsreporturl}',$dpsreport_url
                                    } elseif($default_tag -like '*{dpsreporturl}*' -AND !($dpsreport_url)) {
                                        $default_tag = $default_tag -replace '{dpsreporturl}',''
                                    }
                                    if($default_tag) {
                                        $tags = "-F ""tags=$default_tag"""
                                        Write-Debug "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[GW2Raidar][Tags] $default_tag"
                                    }
                                }
                                catch {
                                    Write-Host -ForegroundColor Red "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[GW2Raidar][Tags][ERROR] $error"
                                    Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[GW2Raidar][Tags][ERROR] $error"
                                    $error.clear()
                                }
                                # CATEGORY
                                try {
                                    if($config.gw2raidar.category.ask_on_upload -in $yes -AND $config.gw2raidar.category.enable -in $yes) {
                                        function Get-Category {
                                            Write-Host "(0) None | (1) Guild / Static | (2) Training | (3) PUG | (4) Low Man / Sells"
                                            $input = Read-Host 'Select temporary category'
                                            switch ($input) {
                                                '' {$selection=''}
                                                '0' {$selection=''}
                                                '1' {$selection='1'}
                                                '2' {$selection='2'}
                                                '3' {$selection='3'}
                                                '4' {$selection='4'}
                                                default {$selection = Get-Category}
                                            }
                                            return $selection
                                        }
                                        $default_category = Get-Category
                                    } elseif($config.gw2raidar.category.ask_on_start -in $yes -AND ($data.gw2raidar.category.temp_stored -replace ' ','') -ne '') {
                                        $default_category = $data.gw2raidar.category.temp_stored
                                    } else {
                                        $default_category = $config.gw2raidar.category.default
                                    }
                                    if($default_category) {
                                        $category = "-F ""category=$default_category"""
                                        Write-Debug "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[GW2Raidar][category] $default_category"
                                    }
                                }
                                catch {
                                    Write-Host -ForegroundColor Red "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[GW2Raidar][Category][ERROR] $error"
                                    Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[GW2Raidar][Category][ERROR] $error"
                                    $error.clear()
                                }
                                # UPLOAD
                                Write-Debug "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[GW2Raidar][Upload] curl.exe -s -X PUT -H ""Authorization: Token $($data.gw2raidar.token)"" $category $tags -F ""file=@$path$extension"" https://www.gw2raidar.com/api/v2/encounters/new | ConvertFrom-Json"
                                try {    
                                    $answer = curl.exe -s -X PUT -H "Authorization: Token $($data.gw2raidar.token)" $category $tags -F "file=@$path$extension" https://www.gw2raidar.com/api/v2/encounters/new | ConvertFrom-Json
                                    Write-Host "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[GW2Raidar][Upload] Successful"
                                    Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[GW2Raidar][Upload] Successful"
                                }
                                catch {
                                    Write-Host -ForegroundColor Red "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[GW2Raidar][Upload][ERROR] $error"
                                    Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[GW2Raidar][Upload][ERROR] $error"
                                    $error.clear()
                                }
                            }
                        } else {
                            Write-Host "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[GW2Raidar] Log ignored because duration <60s"
                            Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[GW2Raidar] Log ignored because duration <60s"
                        }
                    } else {
                        Write-Host "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[GW2Raidar] Log ignored because boss beeing a Kitty-Golem"
                        Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[GW2Raidar] Log ignored because boss beeing a Kitty-Golem"
                    }
                }
                
                function Log-Rename {
                    Write-Verbose "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[Log Renamer] Initialized"
                    Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[Log Renamer] Initialized"
                    try {
                        $format = $config.logrenamer.format
                        $filename = $file_name[-1]
                        $new_name = $format -replace '{duration}',$($sap.duration_f) -replace '{boss}',$($sap.boss) -replace '{boss:l}',($($sap.boss).toLower()) -replace '{boss:u}',($($sap.boss).toUpper()) -replace '{state}',$($sap.state) -replace '{state:l}',($($sap.state).toLower()) -replace '{state:u}',($($sap.state).toUpper()) -replace '{filename}',$filename -replace '{guild}',$($sap.guild_name) -replace '{guild:l}',($($sap.guild_name).toLower()) -replace '{guild:u}',($($sap.guild_name).toUpper()) -replace '{guildtag}',$($sap.guild_tag) -replace '{character}',$($sap.character) -replace '{character:u}',($($sap.character).toUpper()) -replace '{character:l}',($($sap.character).toLower()) -replace '\\',' ' -replace '/',' ' -replace ':',' ' -replace '\*',' ' -replace '"',' ' -replace '<',' ' -replace '>',' ' -replace '\|',' ' -replace '_',' '
                        $rename_path = "$path$extension"
                        Rename-Item -Path "$rename_path" -NewName "$new_name$extension"
                        Write-Host "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[Log Renamer] Log renamed to $new_name"
                        Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[Log Renamer] Log renamed to $new_name"
                    }
                    catch {
                        Write-Host -ForegroundColor Red "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[Log Renamer][ERROR] $error"
                        Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[Log Renamer][ERROR] $error"
                        $error.clear()
                    }
                }
                
                function Invoke-ClientQuerry([string]$apikey,[string[]]$commands){
                    try {
                        Write-Verbose "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[DPS Report][Teamspeak][ClientQuerry] Initialized"
                        Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[DPS Report][Teamspeak][ClientQuerry] Initialized"
                        $tcpclient = new-object System.Net.Sockets.TcpClient("localhost",25639)
                        if ($tcpClient.Connected){
                            Write-Verbose "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[DPS Report][Teamspeak][ClientQuerry] Connected"
                            Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[DPS Report][Teamspeak][ClientQuerry] Connected"
                            $stream = $tcpclient.GetStream()
                            $writer = new-Object System.IO.StreamWriter($stream)
                            $buffer = new-Object Byte[] 1024
                            $encoding = new-Object System.Text.ASCIIEncoding
                            # Login
                            $writer.WriteLine("auth apikey=$apikey")
                            $writer.Flush()
                            sleep -Milliseconds 1000
                            # invoke remote commands
                            $commands | %{
                                $writer.WriteLine($_)
                                $writer.Flush()
                                sleep -Milliseconds 1000
                            }
                            # release resources
                            $writer.Close();$writer.Dispose()
                        }else{
                            Write-Host -ForegroundColor Orange "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[DPS Report][Teamspeak][ClientQuerry][WARNING] Could not Connect"
                            Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[DPS Report][Teamspeak][ClientQuerry][WARNING] Could not Connect"
                        }
                        $tcpclient.Close()
                        $tcpclient.Dispose()
                    }
                    catch {
                        Write-Host -ForegroundColor Red "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[DPS Report][Teamspeak][ClientQuerry][ERROR] $error"
                        Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[DPS Report][Teamspeak][ClientQuerry][ERROR] $error"
                        $error.clear()
                    }
                }

                #### EXECUTION ####
                # LOAD CONFIG/DATA
                try {
                    $config = Get-Content -Path '.\config.json' -Raw | ConvertFrom-Json
                    $data = Get-Content -Path '.\data.json' -Raw | ConvertFrom-Json
                }
                catch {
                    (New-Object -ComObject Wscript.Shell).Popup("$error",0,"Log Uploader - File System Watcher",0+48)
                    Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[File System Watcher][ERROR] $error"
                    $error.clear()
                    exit
                }
                # FILESYSTEMWATCHER VARIABLES
                $path = $Event.SourceEventArgs.FullPath
                $file = $Event.SourceEventArgs.Name
                $file_name = $file.Split('\')
                Write-Verbose "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[File System Watcher] File creation detected: $file"
                Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[File System Watcher] File creation detected: $file"
                # FILE FILTER
                $item = Get-Item "$($path -replace '\[','``[' -replace '\]','``]')*"
                if ($item -is [System.IO.DirectoryInfo]) {
                    Write-Host "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[File-Filter] New folder was created: $($file_name[-1])"
                    Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[File-Filter] New folder was created: $($file_name[-1])"
                } elseif($file -like '*.evtc.zip') {
                    Write-Verbose "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[File System Watcher] Trigger by compressed log file, ignore"
                    Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[File System Watcher] Trigger by compressed log file, ignore"
                } else {
                    if(!($item.Extension)) {
                        $counter = 0
                        Do {
                            echo $counter
                            $item = Get-Item "$($path -replace '\[','``[' -replace '\]','``]')*"
                            $counter++
                            sleep(2)
                        }
                        Until($item.Extension -OR $counter -ge 8)
                    }
                    if($item.Extension -eq '.evtc') {
                        $extension = '.evtc'
                        Write-Verbose "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[File-Filter] New EVTC file detected $file$extension"
                        Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[File-Filter] New EVTC file detected $file$extension"
                        # SIMPLE ARC PARSE
                        $sap = Get-Parse-Data
                        # COMPRESS
                        if($config.main.compress_log -in $yes) {
                            $extension = Compress-Log
                        }
                        # DPS.REPORT
                        if($config.dpsreport.enable -in $yes) {
                            $dpsreport_url = Upload-DPSReport
                        }
                        # GW2RAIDAR
                        if($config.gw2raidar.enable -in $yes) {
                            Upload-GW2Raidar
                        }
                        # RENAME
                        if($config.logrenamer.enable -in $yes) {
                            Log-Rename
                        }
                    } elseif($item.Extension -eq '.zevtc') {
                        Write-Host "Please deactivate compression in ArcDps"
                        Write-Host "If you wish file compression activate ""compress_log"" in the config file"
                        Write-Verbose "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[File-Filter] New ZEVTC file detected $file$extension"
                        Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[File-Filter] New ZEVTC file detected $file$extension"
                    } elseif($item.Extension) {
                        Write-Host -ForegroundColor Orange "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[File-Filter][WARNING] No file with accepted extension ($($item.Extension)) detected"
                        Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[File-Filter][WARNING] No file with accepted extension ($($item.Extension)) detected"
                    } else {
                        Write-Host -ForegroundColor Orange "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[File-Filter][WARNING] File without extension detected"
                        Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[File-Filter][WARNING] File without extension detected"
                    }
                }
                # "TRY" SEPARATOR
                if(!($file -like '*.evtc.zip*')) {
                    Write-Host -ForegroundColor Yellow "#######################################################################################################################"
                    Write-Host "Log Uploader(v$version)is ready!"
                }
                Add-Content './log.txt' "#######################################################################################################################"
                #### END ####
              }
    Register-ObjectEvent $watcher "Created" -Action $action | Out-Null
    Write-Host "Log Uploader(v$version) is ready!"
    while ($true) {sleep 5}
}

#### START ####
try {
    # CREATE DATA FILE
    if(!(Test-Path -Path './data.json')) {
        Create-Data
    }
    
    # LOAD DATA FILE
    $data = Get-Content -Path '.\data.json' -Raw | ConvertFrom-Json
    
    # CREATE CONFIG FILE
    if(!(Test-Path -Path './config.json')) {
        Create-Config
    }
    
    # CHECK IF FIRST START
    if((Get-FileHash -Path '.\config.json'-Algorithm md5).hash -eq $config_md5) {
        Start-Process notepad './config.json'
        exit
    }
    
    # LOAD CONFIG FILE
    $config = Get-Content -Path '.\config.json' -Raw | ConvertFrom-Json
    
    # CHECK IF NEEDED TO RUN
    if(($config.gw2raidar.enable -replace ' ','') -in $no -AND ($config.dpsreport.enable -replace ' ','') -in $no -AND ($config.renamer.enable -replace ' ','') -in $no) {
        exit
    }
    
    # VERBOSE
    if($config.main.verbose -in $yes) {
        $VerbosePreference = "Continue"
    } else {
        $VerbosePreference = "SilentlyContinue"
    }
    
    # DEBUG
    if($config.main.debug -in $yes) {
        $DebugPreference = "Continue"
    } else {
        $DebugPreference = "SilentlyContinue"
    }
    
    # UPDATE APP
    if($config.main.autoupdate -in $yes) {
        try {
            $answer = Invoke-WebRequest -Uri 'https://apps.reyko.online/log_uploader/log_uploader.md5sum'
            $md5 = $answer -replace '\s',''
            if($md5 -ne ((Get-FileHash -Path './Log Uploader.exe' -Algorithm md5).hash)) {
                $answer = (New-Object -ComObject Wscript.Shell).Popup("There is an update avalible`nWant to install it now?",0,"Log Uploader - Update",4+32)
                if($answer -eq 6) {
                    Add-Content './updater.ps1' $update_script
                    powershell.exe -NoLogo -ExecutionPolicy Bypass -File ./updater.ps1
                }
            }
        }
        catch {
            Write-Host -ForegroundColor Red "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[Updater][ERROR] $error"
            Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[Updater][ERROR] $error"
            $error.clear()
        }
    }
    # UPDATE DATA FILE
    if($data.main.data_version -ne $data_md5) {
        Update-Data
    }
    # UPDATE CONFIG CONFIG
    if($data.main.config_version -ne $config_md5) {
        Update-Config
    }

    # UPDATE INSTALL SIMPLE ARC PARSE
    if(!(Test-Path -Path './simpleArcParse.exe') -OR (./simpleArcParse.exe version) -ne $req_sap_ver) {
        try {
            [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
            $simplearcparse = Invoke-WebRequest -Uri $req_sap_uri
            Set-Content -Path './simplearcparse.exe' -Encoding Byte -Value $simplearcparse.Content -Force
            Write-Verbose "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[SimpleArcParse] Install/Update successful"
            Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[SimpleArcParse] Install/Update successful"
        }
        catch {
            Write-Host -ForegroundColor Red "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[SimpleArcParse][ERROR] $error"
            Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[SimpleArcParse][ERROR] $error"
            Add-Content './log.txt' "#######################################################################################################################"
            $error.clear()
            exit
        }
    }
    
    # GW2 API TOKEN
    if(($data.guildwars2.token -replace ' ','') -eq '') {
        Write-Host 'Please insert your Gw2 Api token. Get it at "https://account.arena.net/applications"'
        Write-Host 'Only "Account" is needed.'
        $data.guildwars2.token = Get-Gw2Token
        $data | ConvertTo-Json | Set-Content './data.json'
        Write-Verbose "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[Gw2 Api] Token set up"
        Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[GW2 Api] Token set up"
    }
    
    # DPS.REPORT SETTINGS
    if($config.dpsreport.enable -in $yes) {
        # DPS.REPORT TOKEN
        if(($data.dpsreport.token -replace ' ','') -eq '') {
            start 'https:/dps.report/getUsertoken'
            Write-Host 'No dps.report token found. Please go to https://dps.report/getUserToken to get your token.'
            $data.dpsreport.token = Read-Host 'Token'
            $data | ConvertTo-Json | Set-Content './data.json'
            Write-Verbose "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[DPS Report] Token set up"
            Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[DPS Report] Token set up"
        }
        # TEAMSPEAK TOKEN
        if(($data.dpsreport.teamspeak_token -replace ' ','') -eq '') {
            Write-Host "No Teamspeak ClientQuerry Apikey found. Please get it in Teamspeak: Tools > Options > Addons > ClientQuerry > Settings."
            Write-Host "Make sure that the Teamspeak Addon is enabled."
            $data.dpsreport.teamspeak_token = Read-Host 'Token'
            $data | ConvertTo-Json | Set-Content './data.json'
            Write-Verbose "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[DPS Report][Teamspeak] Token set up"
            Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[DPS Report][Teamspeak] Token set up"
        }
    }
    
    # GW2RAIDAR SETTINGS
    if($config.gw2raidar.enable -in $yes) {
        if(($data.gw2raidar.token -replace ' ','') -eq '') {
            Write-Host 'No GW2Raidar token found. Please entry your credentials to get your token.'
            Get-Raidar-Token
        }
        # CATEGORY
        if($config.gw2raidar.category.ask_on_start -in $yes -AND $config.gw2raidar.category.enable -in $yes ) {
            function Get-Category {
                Write-Host "(0) None | (1) Guild / Static | (2) Training | (3) PUG | (4) Low Man / Sells"
                $input = Read-Host 'Select temporary category'
                switch ($input) {
                    '' {$selection=''}
                    '0' {$selection=''}
                    '1' {$selection='1'}
                    '2' {$selection='2'}
                    '3' {$selection='3'}
                    '4' {$selection='4'}
                    default {$selection = Get-Category}
                }
                return $selection
            }
            $data.gw2raidar.category.temp_stored = Get-Category
            $data | ConvertTo-Json | Set-Content './data.json'
        }
        # TAG
        if($config.gw2raidar.tag.ask_on_start -in $yes -AND $config.gw2raidar.tag.enable -in $yes) {
            if(($config.gw2raidar.tag.selection_preset -replace ' ','') -ne '') {
                $preset = ($config.gw2raidar.tag.selection_preset).Split(';')
                $display = ''
                $counter = 1
                foreach($tag IN $preset) {
                    $display += "($counter)$tag "
                    $counter++
                }
                Write-Host "Predefinied set of tags: $display"
                $input = Read-Host 'Select preset or enter temporary tag'
                if($input -match "^\d+$" -AND [int]$input -le $counter) {
                    $selection = $preset[($input-1)]
                } else {
                    $selection = $input
                }
            } else {
                $selection = Read-Host 'Enter temporary tag'
            }
            $data.gw2raidar.tag.temp_stored = $selection
            $data | ConvertTo-Json | Set-Content './data.json'
        }
    }
    
    # START FILE SYSTEM WATCHER
    Start-Log-Uploader
}
catch {
    (New-Object -ComObject Wscript.Shell).Popup("$error",0,"Log Uploader",0+48)
    Add-Content './log.txt' "$(get-date -uformat "[%m/%d/%Y %H:%M:%S]")[ERROR] $error"
    Add-Content './log.txt' "#######################################################################################################################"
    $error.clear()
    exit
}